<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Utilitaire extends Model
{
    //
    public static function getContent($id){
        $req = "select * from contenu where id = '%s'";
        $req = sprintf($req, $id);
        $result =  DB::connection('mysql')->select($req);
        return $result[0];
    }

    public static function getProduit(){
        $query = "select s.id, s.nomService, s.prix, s.star, s.review, s.days, s.night, i.nomimage from service as s join imageService as imgs on s.id = imgs.idservice join image as i on imgs.idimage = i.id where imgs.estValide = 1";
        $result =  DB::connection('mysql')->select($query);
//        var_dump($result);
        return $result;
    }

    public static function seConnecter($log, $pass){
        $req = "select * from utilisateur where login = '%s' and password = '%s'";
        $req = sprintf($req, $log, $pass);
        $row =  DB::connection('mysql')->select($req);
        return $row;
    }

    public static function modifier($id, $nomTable, $modif){
        $retour = DB::table($nomTable)
            ->where('id', $id)
            ->update(['contenu' => $modif]);
        var_dump($retour);
//        if($retour == 1){
//            redirect('index.php/Backoffice');
//        }
//        if($retour != 1){
//            redirect('index.php/Backoffice');
//        }
    }

    public static function getSectionPage(){
        $req = "select * from sectionPage";
        $result =  DB::connection('mysql')->select($req);
        return $result;
    }

    public static function getlisteContenu($id){
        $req = "select id, nomContenu from contenu where idsection = '%s'";
        $req = sprintf($req, $id);
        $result =  DB::connection('mysql')->select($req);
        return $result;
    }

    public static function getService($id){
        $req = "select * from service where id = '%s'";
        $req = sprintf($req, $id);
        $result =  DB::connection('mysql')->select($req);
        return $result;
    }

    public static function getImageService($idservice){
        $req = "select * from imageService where idservice = '%s'";
        $req = sprintf($req, $idservice);
        $result =  DB::connection('mysql')->select($req);
        return $result;
    }

    public static function updateservice($id, $nomService, $prix, $star, $review, $days, $night, $idimage){
        $req1 = "update service set nomService = '%s', prix = '%s', star = '%s', review = '%s', days = '%s', night = '%s' where id = '%s'";
        $req1 = sprintf($req1, $nomService, $prix, $star, $review, $days, $night, $id);
        $retour1 = DB::update($req1);

        $req2 = "update imageservice set estvalide = 1 where idimage = '%s' and idservice = '%s'";
        $req2 = sprintf($req2, $idimage, $id);
        $retour2 = DB::update($req2);

        $req3 = "update imageService set estValide = 0 where idimage != '%s' and idservice = '%s'";
        $req3 = sprintf($req3, $idimage, $id);
        $retour3 = DB::update($req3);

        return $retour1+$retour2+$retour3;
    }
}
